package buscaminas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Clase para generar i ejectutar tableros de Buscaminas.
 * 
 * @author Izan Blanco Segovia
 * @version 31/01/2023
 */

public class Buscaminas {
	static Scanner sc = new Scanner(System.in);
	static Random rd = new Random();
	static HashMap<Integer, Character> conversion = new HashMap<>() {
		{
			put(0, ' ');
			put(9, '#');
			put(1, '1');
			put(2, '2');
			put(3, '3');
			put(4, '4');
			put(5, '5');
			put(6, '6');
			put(7, '7');
			put(8, '8');
			put(-1, '?');
			put(-2, '*');
		}
	};
	static int[][] tableroHidden;
	static int[][] tableroShown;
	static int[][] tableroVisited;
	static List<int[]> direcciones = new ArrayList<>() {
		{
			add(new int[] { -1, -1 });
			add(new int[] { -1, 0 });
			add(new int[] { -1, 1 });
			add(new int[] { 0, -1 });
			add(new int[] { 0, 1 });
			add(new int[] { 1, -1 });
			add(new int[] { 1, 0 });
			add(new int[] { 1, 1 });
		}
	};
	static boolean done = false;
	static boolean dead = false;
	static int nMinas;

	public static void main(String[] args) {
		int fT = 8;
		int fC = 8;
		nMinas = 10;
		System.out.println(
				"DIFICULTAD\n1-PRINCIPIANTE (8 x 8, 10 MINAS)\n2-INTERMIDIARIO (16 x 16, 40 MINAS)\n3-EXPERTO (16 x 30, 99 MINAS)\n4-PERSONALIZADO");
		int dificultad = sc.nextInt();
		clearScreen();
		switch (dificultad) {
		case 1:
			fT = 8;
			fC = 8;
			nMinas = 10;
			break;
		case 2:
			fT = 16;
			fC = 16;
			nMinas = 40;
			break;
		case 3:
			fT = 16;
			fC = 30;
			nMinas = 99;
			break;
		case 4:
			System.out.print("Filas: ");
			fT = sc.nextInt();
			System.out.print("\n");
			System.out.print("Columnes: ");
			fC = sc.nextInt();
			System.out.print("\n");
			System.out.println("Nombre de mines: ");
			nMinas = sc.nextInt();
			break;
		}
		clearScreen();
		tableroHidden = new int[fT][fC];
		tableroShown = new int[fT][fC];
		tableroVisited = new int[fT][fC];
		int nMinasSinMarcar = nMinas;
		fillTablero(tableroHidden, 0);
		fillTablero(tableroShown, 9);
		fillTablero(tableroVisited, 0);
		printTablero(tableroShown);
		System.out.print("Posicio Inicial: ");
		int[] posicionInicial = new int[] { sc.nextInt(), sc.nextInt() };

		// INICIAR

		tableroHidden[posicionInicial[0]][posicionInicial[1]] = 1;
		colocarMinas(tableroHidden, nMinas);
		tableroHidden[posicionInicial[0]][posicionInicial[1]] = 0;
		revelarCelda(new ArrayList<int[]>() {
			{
				add(posicionInicial);
			}
		});

		while (!done) {
			clearScreen();
			System.out.println("Nº minas sense marcar: " + nMinasSinMarcar);
			printTablero(tableroShown);
			// printTablero(tableroHidden);
			System.out.println("1-Revelar\n2-Marcar");
			int opcion = sc.nextInt();
			System.out.print(String.format("Posicio per a %s: ", (opcion == 1) ? "revelar" : "marcar/desmarcar"));
			int[] seleccion = new int[] { sc.nextInt(), sc.nextInt() };
			if (opcion == 1) {
				if (tableroHidden[seleccion[0]][seleccion[1]] == 1) {
					done = true;
					dead = true;
					break;
				}
				revelarCelda(new ArrayList<int[]>() {
					{
						add(seleccion);
					}
				});
			} else {
				if (tableroShown[seleccion[0]][seleccion[1]] == -1 || tableroShown[seleccion[0]][seleccion[1]] == 9) {
					if (tableroShown[seleccion[0]][seleccion[1]] == -1) {
						nMinasSinMarcar++;
						tableroShown[seleccion[0]][seleccion[1]] = 9;
					} else {
						nMinasSinMarcar--;
						tableroShown[seleccion[0]][seleccion[1]] = -1;
					}
				}
				done = checkForCompletion();
			}
		}
		clearScreen();
		if (dead) {
			for (int _a = 0; _a < tableroHidden.length; _a++) {
				for (int _b = 0; _b < tableroHidden[_a].length; _b++) {
					if (tableroHidden[_a][_b] == 1) {
						tableroShown[_a][_b] = -2;
					}
				}
			}
			System.out.println("Has mort!");
			printTablero(tableroShown);
		} else {
			System.out.println("Has guanyat!");
			printTablero(tableroShown);
		}
	}

	/**
	 * Coloca mines en posicions legals dins d'un tauler proporcionat.
	 * 
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 * @param tableroIn el tauler sobre el que col·locar mines.
	 * @param nMinas    el nombre de mines a col·locar
	 */
	public static void colocarMinas(int[][] tableroIn, int nMinas) {
		ArrayList<int[]> posicionesPossibles = new ArrayList<>();
		for (int _a = 0; _a < tableroIn.length; _a++) {
			for (int _b = 0; _b < tableroIn[_a].length; _b++) {
				if (tableroIn[_a][_b] != 1) {
					posicionesPossibles.add(new int[] { _a, _b });
				}
			}
		}
		for (int _a = 0; _a < nMinas; _a++) {
			int[] posicionElegida = posicionesPossibles.get(rd.nextInt(0, posicionesPossibles.size()));
			tableroIn[posicionElegida[0]][posicionElegida[1]] = 1;
			posicionesPossibles.remove(posicionElegida);
		}
	}

	/**
	 * Imprimeix el tauler amb un format adaptat al joc de Buscamines.
	 * 
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 * @param tableroIn el tauler que imprimir.
	 */
	public static void printTablero(int[][] tableroIn) {
		for (int _a = 0; _a < tableroIn.length; _a++) {
			System.out.println("┌───┐ ".repeat(tableroIn[0].length));
			for (int _b = 0; _b < tableroIn[_a].length; _b++) {
				System.out.print("│ " + conversion.get(+tableroIn[_a][_b]) + " │ ");
			}
			System.out.print("\n");
			System.out.println("└───┘ ".repeat(tableroIn[0].length));
		}

	}

	/**
	 * Comprova que totes les mines han sigut marcades i que no hi ha cap altre
	 * casella marcada que no sigui una mina.
	 * 
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 * @return Un boolean representant si la partida a acabat en victoria o encara
	 *         no ha terminat.
	 */
	public static boolean checkForCompletion() {
		List<int[]> posicionesMarcadas = new ArrayList<>();
		int minasMarcadas = 0;
		int minasEncontradas = 0;
		for (int _a = 0; _a < tableroShown.length; _a++) {
			for (int _b = 0; _b < tableroShown[_a].length; _b++) {
				if (tableroShown[_a][_b] == -1) {
					minasMarcadas++;
					posicionesMarcadas.add(new int[] { _a, _b });
				}
			}
		}
		for (int[] posicion : posicionesMarcadas) {
			if (tableroHidden[posicion[0]][posicion[1]] == 1) {
				minasEncontradas++;
			}
		}
		if (minasEncontradas == nMinas && minasMarcadas == nMinas) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Obteneix les cel·les adjaçents a una posicio determinada.
	 * 
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 * @param tablero  el tauler on buscarem les cel·les.
	 * @param posicion la posicio on mirarem.
	 * @return Les cel·les adjacents a la posicio proporcionada.
	 */
	public static List<int[]> obtenerCeldasAdjacentes(int[][] tablero, int[] posicion) {
		List<int[]> resultado = new ArrayList<>();
		for (int[] direccion : direcciones) {
			int cx = posicion[1] + direccion[1];
			int cy = posicion[0] + direccion[0];
			if (cy >= 0 && cy < tablero.length)
				if (cx >= 0 && cx < tablero[cy].length) {
					if (tableroShown[cy][cx] == 9 || tableroShown[cy][cx] == -1) {
						resultado.add(new int[] { cy, cx });
					}
				}
		}
		return resultado;
	}

	/**
	 * Funcio recursiva que revel·la les posicions proporcionades, si no te cap mina
	 * adjaçent es crida a si mateixa amb les cel·les adjaçents com a parametre.
	 * 
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 * @param posiciones la llista de posicions on revel·lar el contingut de la
	 *                   cel·la.
	 */
	public static void revelarCelda(List<int[]> posiciones) {
		for (int[] posicion : posiciones) {
			int minas = 0;
			List<int[]> nuevasPosiciones = obtenerCeldasAdjacentes(tableroHidden, posicion);
			for (int[] nuevaPosicion : nuevasPosiciones) {
				if (tableroHidden[nuevaPosicion[0]][nuevaPosicion[1]] == 1) {
					minas++;
				}
			}
			tableroShown[posicion[0]][posicion[1]] = minas;
			if (minas == 0) {
				revelarCelda(nuevasPosiciones);
			}
		}
	}

	/**
	 * Emplena el tauler proporcionat amb el numero proporcionat.
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 * @param tableroIn el taulell a emplenar.
	 * @param simbolo el numero que repetir en totes les cel·les del taulell.
	 */
	public static void fillTablero(int[][] tableroIn, int simbolo) {
		for (int _a = 0; _a < tableroIn.length; _a++) {
			for (int _b = 0; _b < tableroIn[_a].length; _b++) {
				tableroIn[_a][_b] = simbolo;
			}
		}
	}
	
	/**
	 * Imprimeix un munt de salt de linias per moure el anterior text fora de vista i aixi limpiant la consola.
	 * @author Izan Blanco Segovia
	 * @version 31/01/2023
	 */
	public static void clearScreen() {
		System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
}
