package problemasInventados;

import java.util.ArrayList;
import java.util.Scanner;

public class StackMarker {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int casos = sc.nextInt();
		String res = "";

		while (casos-- > 0) {
			int attackPower = sc.nextInt();
			int nAttacks = sc.nextInt();
			Position attackPos = new Position(sc.nextInt(), sc.nextInt());
			int attackRadius = sc.nextInt();
			int nPlayers = sc.nextInt();
			ArrayList<Player> playerList = new ArrayList<>();
			sc.nextLine();

			while (nPlayers-- > 0) {
				String name = sc.nextLine();
				Position pos = new Position(sc.nextInt(), sc.nextInt());
				int lvl = sc.nextInt();
				int hp = sc.nextInt();
				int def = sc.nextInt();
				playerList.add(new Player(pos, name, lvl, hp, def));
				sc.nextLine();
			}

			for (int x = 0; x < nAttacks; x++) {
				ArrayList<Player> playersHit = getPlayersHit(attackPos, attackRadius, playerList);
				// Si no s'ha golpejat cap jugador hi ha que matar a tots, aixo ho faix restant
				// la seva vida, pero nomes si son vius, ja que si no canviaria erroniament el
				// valor de "NeededHp".
				if (playersHit.size() == 0) {
					for (Player pl : playerList) {
						if (pl.alive) {
							pl.getDamaged(pl.hp);
						}
					}
				}
				for (Player pl : playersHit) {
					dealDamage(pl, attackPower, playersHit.size());
				}

			}
			res += (getPlayersAlive(playerList) > 0) ? "PROG" : "WIPE";
			res += "\n" + displayAllPlayers(playerList) + "\n";
		}

		System.out.println(res);
	}

	static public ArrayList<Player> getPlayersHit(Position attackPosition, int attackRadius,
			ArrayList<Player> playerList) {
		ArrayList<Player> result = new ArrayList<>();
		for (Player pl : playerList) {
			if (pl.pos.x <= attackPosition.x + attackRadius && pl.pos.x >= attackPosition.x - attackRadius) {
				if (pl.pos.y <= attackPosition.y + attackRadius && pl.pos.y >= attackPosition.y - attackRadius) {
					if (pl.alive) {
						result.add(pl);
					}
				}
			}
		}
		return result;
	}

	static public int getPlayersAlive(ArrayList<Player> playerList) {
		int res = 0;
		for (Player player : playerList) {
			if (player.alive) {
				res++;
			}
		}
		return res;
	}

	static public String displayAllPlayers(ArrayList<Player> playerList) {
		String res = "";
		for (Player player : playerList) {
			res += player.toString() + "\n";
		}
		return res;
	}

	static public void dealDamage(Player victim, double attackPower, double nPlayersStruck) {
		double damageDealt = Math.max((attackPower / nPlayersStruck + (((nPlayersStruck + 1) / 2) - 1))
				- (victim.def * ((victim.lvl - 1) / (80 - 1))), 0);
		victim.getDamaged((int) damageDealt);
	}

	static public class Player {
		Position pos;
		String name;
		double lvl;
		int hpMax;
		int hp;
		int neededHp;
		double def;
		boolean alive;

		public Player(Position pos, String name, double lvl, int hpMax, double def) {
			this.pos = pos;
			this.name = name;
			this.lvl = lvl;
			this.hpMax = hpMax;
			this.hp = hpMax;
			this.neededHp = 0;
			this.def = def;
			this.alive = true;
		}

		@Override
		public String toString() {
			return String.format("%s-%d-%s-%d", name, (int) lvl, getHPText(), neededHp);
		}

		public String getHPText() {
			if (alive) {
				return String.format("%d/%dHP", hp, hpMax);
			}
			return "MORT";
		}

		public void getDamaged(int damage) {
			this.hp -= damage;

			if (this.hp <= 0) {
				// Si mors la vida que haguessis necessitat per a sobreviure es la teva vida
				// restant en positiu +1;
				this.alive = false;
				this.neededHp = (this.hp * -1) + 1;
			}
		}
	}

	static public class Position {
		int x;
		int y;

		public Position(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public String toString() {
			return x + " " + y;
		}

	}

}
