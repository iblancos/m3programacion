package problemasInventados;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class StackMarkerGen {
	static Scanner sc = new Scanner(System.in);
	static Random rd = new Random();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			for(int x = 0;x<5;x++) {
				createFiles(x);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void createFiles(int calvario) throws IOException {
		int casos = sc.nextInt();
		String res = "";
		FileWriter inWriter = new FileWriter(String.format("E:\\M3\\Projectos\\src\\problemasInventados\\caso%dIn.in", calvario), true);
		FileWriter outWriter = new FileWriter(String.format("E:\\M3\\Projectos\\src\\problemasInventados\\caso%dOut.out", calvario), true);
		inWriter.write(casos + "\n");

		while (casos-- > 0) {
			int attackPower = rd.nextInt(0, 1000001);
			int nAttacks = rd.nextInt(0, 6);
			Position attackPos = new Position(rd.nextInt(-5, 6), rd.nextInt(-5, 6));
			int attackRadius = rd.nextInt(3, 6);
			int nPlayers = rd.nextInt(1, 9);
			ArrayList<Player> playerList = new ArrayList<>();
			ArrayList<String> names = new ArrayList<>() {
				{
					add("GODOFREDO");
					add("EUSTAQUIO");
					add("EZEQUIEL");
					add("DEOGRACIAS");
					add("EXPOSITO");
					add("MELQUIADES");
					add("MELCHOR");
					add("DOMINGO");
					add("ALTAGRACIA");
					add("BLADISLABO");
					add("CECILIO");
					add("CASIMIRO");
					add("CRISANTA");
					add("DIONISIO");
					add("DAVINIA");
					add("DEMETRIA");
					add("EDELMIRA");
					add("EDELBERTO");
					add("EUFRASIO");
					add("FREDESVINDA");
					add("GENOVEVO");
					add("GREGORIO");
					add("GUMERSINDO");
					add("HELIMENAS");
					add("HOMOBONO");
					add("HILARIO");
					add("HUMILDE");
					add("ILORA");
					add("IFIGENIA");
					add("JUSTINA");
					add("LICINIA");
					add("LEOVIGILDO");
					add("MICAELA");
					add("MINERVIO");
					add("NEPOMUCENA");
					add("NEMESIO");
					add("NICOMEDES");
					add("PERFECTO");
					add("SILVESTRA");
					add("SISEBUTA");
					add("TEODOMIRA");
					add("TEOPISTO");
					add("TRANQUILINO");
					add("TRINITARIO");
					add("VERIDIANO");
				}
			};
			inWriter.write(attackPower + "\n" + nAttacks + "\n" + attackPos + "\n" + attackRadius + "\n");
			inWriter.write(nPlayers + "\n");

			while (nPlayers-- > 0) {
				String name = names.get(rd.nextInt(0, names.size()));
				names.remove(name);
				Position pos = new Position(rd.nextInt(-5, 6), rd.nextInt(-5, 6));
				int lvl = rd.nextInt(1, 81);
				int hp = rd.nextInt(1, 100000);
				int def = rd.nextInt(0, 10001);
				playerList.add(new Player(pos, name, lvl, hp, def, true));
				inWriter.write(name + "\n" + pos.toString() + "\n" + lvl + "\n" + hp + "\n" + def + "\n");
			}

			for (int x = 0; x < nAttacks; x++) {
				ArrayList<Player> playersHit = getPlayersHit(attackPos, attackRadius, playerList);
				// Si no s'ha golpejat cap jugador hi ha que matar a tots, aixo ho faix restant
				// la seva vida, pero nomes si son vius, ja que si no canviaria erroniament el
				// valor de "NeededHp".
				if (playersHit.size() == 0) {
					for (Player pl : playerList) {
						if (pl.alive) {
							pl.getDamaged(pl.hp);
						}
					}
				}
				for (Player pl : playersHit) {
					dealDamage(pl, attackPower, playersHit.size());
				}

			}
			res += (getPlayersAlive(playerList) > 0) ? "PROG" : "WIPE";
			res += "\n" + displayAllPlayers(playerList);
			res += "\n";
		}
		
		outWriter.write(res);
		inWriter.close();
		outWriter.close();

	}

	static public ArrayList<Player> getPlayersHit(Position attackPosition, int attackRadius,
			ArrayList<Player> playerList) {
		ArrayList<Player> result = new ArrayList<>();
		for (Player pl : playerList) {
			if (pl.pos.x <= attackPosition.x + attackRadius && pl.pos.x >= attackPosition.x - attackRadius) {
				if (pl.pos.y <= attackPosition.y + attackRadius && pl.pos.y >= attackPosition.y - attackRadius) {
					if (pl.alive) {
						result.add(pl);
					}
				}
			}
		}
		return result;
	}

	static public int getPlayersAlive(ArrayList<Player> playerList) {
		int res = 0;
		for (Player player : playerList) {
			if (player.alive) {
				res++;
			}
		}
		return res;
	}

	static public String displayAllPlayers(ArrayList<Player> playerList) {
		String res = "";
		for (Player player : playerList) {
			res += player.toString() + "\n";
		}
		return res;
	}

	static public void dealDamage(Player victim, double attackPower, double nPlayersStruck) {
		double damageDealt = Math.max((attackPower / nPlayersStruck + (((nPlayersStruck + 1) / 2) - 1))
				- (victim.def * ((victim.lvl - 1) / (80 - 1))), 0);
		victim.getDamaged((int) damageDealt);
	}

	static public class Player {
		Position pos;
		String name;
		double lvl;
		int hpMax;
		int hp;
		int neededHp;
		double def;
		boolean alive;

		public Player(Position pos, String name, double lvl, int hpMax, double def, boolean alive) {
			this.pos = pos;
			this.name = name;
			this.lvl = lvl;
			this.hpMax = hpMax;
			this.hp = hpMax;
			this.neededHp = 0;
			this.def = def;
			this.alive = alive;
		}

		@Override
		public String toString() {
			return String.format("%s-%d-%s-%d", name, (int) lvl, getHPText(), neededHp);
		}

		public String getHPText() {
			if (alive) {
				return String.format("%d/%dHP", hp, hpMax);
			}
			return "MORT";
		}

		public void getDamaged(int damage) {
			this.hp -= damage;

			if (this.hp <= 0) {
				// Si mors la vida que haguessis necessitat per a sobreviure es la teva vida
				// restant en positiu +1;
				this.alive = false;
				this.neededHp = (this.hp * -1) + 1;
			}
		}
	}

	static public class Position {
		int x;
		int y;

		public Position(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public String toString() {
			return x + " " + y;
		}

	}

}
